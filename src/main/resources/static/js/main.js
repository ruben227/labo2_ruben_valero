
$(document).ready(function(){
    if(document.forms.main_form){
        document.forms.main_form.onsubmit = function(){
            if(isNaN(document.forms.main_form.elements.year.value)){
                $("#yearError").removeClass("d-none")
                return false
            }
                
        }
    }
   
    //botones
   // $("#submit").on("click", search)
    $("#reset").on("click", reset)
   // $("form").keypress(function (e) {
     //   if (e.which == 13) {
       //     search()
        //}
    //})
    
    //cambio de idioma
    $("#locales").change(function () {
        var selectedOption = $('#locales').val();
        if (selectedOption != '') {
            window.location.replace('?lang=' + selectedOption);
        }
    });

    
    
    function reset() {
        $("#maincontainer").html("")
        document.main_form.year.value = ""
        document.main_form.title.value = ""
    }
    
})
