package com.movieapi.API;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {
    @GetMapping("/")
    public String Index(){
        return "redirect:/movies";
    }
    
    @GetMapping("/html")
    public String Html(){
        return "fragments/title";
    }
    @GetMapping("/change")
    public String change(){
        return "fragments/changelang";
    }
    

    @GetMapping("/movies")
    public ModelAndView GetFilter(@RequestParam(name ="title", defaultValue = "") String title, @RequestParam(name ="year", defaultValue = "") Integer year){
        
        ModelAndView mv = new ModelAndView("browser");
        mv.addObject("movies", MoviesController.FindByTitleAndYear(title,year));
        return mv;
    }

    @GetMapping("/movies/{id}")
    public ModelAndView GetId(@PathVariable("id") int id){

        ModelAndView mv = new ModelAndView("detail");
        mv.addObject("movie", MoviesController.FindById(id));
        return mv;
    }

}
